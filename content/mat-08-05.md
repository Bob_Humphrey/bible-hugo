---
title: "The centurion"
date: 2018-07-19T14:53:05-04:00
draft: false
book: "Matthew"
chapter_verse: "8:5-13"
sequence: "010805"
tags: ["Matthew", "outsiders"]
---
When he went into Capernaum a centurion came up and pleaded with him.

'Sir,' he said, 'my servant is lying at home paralysed and in great pain.'

Jesus said to him, 'I will come myself and cure him.'

The centurion replied, 'Sir, I am not worthy to have you under my roof; just give the word and my servant will be cured.

For I am under authority myself and have soldiers under me; and I say to one man, "Go," and he goes; to another, "Come here," and he comes; to my servant, "Do this," and he does it.'

When Jesus heard this he was astonished and said to those following him, 'In truth I tell you, in no one in Israel have I found faith as great as this.

And I tell you that many will come from east and west and sit down with Abraham and Isaac and Jacob at the feast in the kingdom of Heaven;

but the children of the kingdom will be thrown out into the darkness outside, where there will be weeping and grinding of teeth.'

And to the centurion Jesus said, 'Go back, then; let this be done for you, as your faith demands.' And the servant was cured at that moment.
